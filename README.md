This repository contains two exercises: frontend (sent by mistake) and backend. The frontend was made in Angular and the backend was made in SpringBoot + JPA(Hibernate) (I already had seen Elixir backend code at frontend exercise).

## Exercise Requirments

* Retrieve data from a single user
    * users are indexed and queried by username
* Retrieve a collection of products
    * products must have a unique identifier, a price and a name
* Placing an order
    * users aren't allowed to place an order if their current balance isn't enough for the order total
    * users aren't allowed to order a product previously ordered
    * orders must record which user is ordering, which products are being ordered and what the total amount is

## Setup
* Java
* Node

## Backend
### Start 

starts at 8080 

```bash
./start_benckend -r
```

### Test
```bash
./start_backend -t
```

You alas find the Postman collection under the banckend folder.

## Frontend

### Start

starts at 4200

```
./start_frontend
```
