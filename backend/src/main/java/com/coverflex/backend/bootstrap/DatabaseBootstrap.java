package com.coverflex.backend.bootstrap;

import com.coverflex.backend.domain.Product;
import com.coverflex.backend.repository.ProductsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
public class DatabaseBootstrap {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseBootstrap.class);

    @Bean
    CommandLineRunner initData(ProductsRepository repository) {
        return args -> {
            LOGGER.info("Inserting Netflix" + repository.save(new Product("netflix", "Netflix", 200)));
            LOGGER.info("Inserting Disney+" + repository.save(new Product("disney", "Disney+", 400)));
            LOGGER.info("Inserting HBO" + repository.save(new Product("hbo", "HBO", 250)));
            LOGGER.info("Inserting Zulu" + repository.save(new Product("zulu", "Zulu", 150)));
            LOGGER.info("Inserting Twitch" + repository.save(new Product("twitch", "Twitch", 350)));
            LOGGER.info("Inserting Youtube" + repository.save(new Product("youtube", "YouTube", 100)));
            LOGGER.info("Inserting Vimeo" + repository.save(new Product("vimeo", "Vimeo", 300)));
        };
    }
}
