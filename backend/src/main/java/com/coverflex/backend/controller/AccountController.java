package com.coverflex.backend.controller;

import com.coverflex.backend.domain.ProductOrder;
import com.coverflex.backend.domain.User;
import com.coverflex.backend.domain.dto.AccountDetailsDTO;
import com.coverflex.backend.repository.OrderRepository;
import com.coverflex.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Validated
public class AccountController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @GetMapping(value = "/users/{username}")
    AccountDetailsDTO details(@PathVariable("username") @NotBlank @Size(max = 20) @Pattern(regexp = "^[aA-zZ0-9]+$") String username) {
        final User user = this
                .userRepository
                .findById(username)
                .orElseGet(() -> this.userRepository.save(new User(username)));

        final List<ProductOrder> products = orderRepository.getOrderedProductsByUser(user);

        return new AccountDetailsDTO(user, products.stream().map(ProductOrder::getProductId).collect(Collectors.toSet()));
    }

}
