package com.coverflex.backend.controller;

import com.coverflex.backend.domain.Order;
import com.coverflex.backend.domain.exception.AlreadyOrderSomeProductsException;
import com.coverflex.backend.domain.exception.InsufficientFundsException;
import com.coverflex.backend.domain.exception.ProductsNotFoundException;
import com.coverflex.backend.domain.request.OrderRequest;
import com.coverflex.backend.framework.api.ApiError;
import com.coverflex.backend.repository.UserRepository;
import com.coverflex.backend.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class OrderController {

    private final static Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    OrderService orderService;

    @PostMapping(value = "/orders", consumes = "application/json", produces = "application/json")
    ResponseEntity<Object> placeOrder(@RequestBody @Validated OrderRequest orderRequest) {

        try {

            final Order order = this.orderService.processOrder(orderRequest.getUserId(), orderRequest.getItems());

            return ResponseEntity.ok(order.toDTO());

        } catch (ProductsNotFoundException exception) {

            return ResponseEntity.badRequest().body(new ApiError(ProductsNotFoundException.ERROR_VALUE));

        } catch (AlreadyOrderSomeProductsException exception) {

            return ResponseEntity.badRequest().body(new ApiError(AlreadyOrderSomeProductsException.ERROR_VALUE));

        } catch (InsufficientFundsException exception) {

            return ResponseEntity.badRequest().body(new ApiError(InsufficientFundsException.ERROR_VALUE));

        } catch (DataIntegrityViolationException exception) {

            LOGGER.warn("Violation attempted", exception);

            return ResponseEntity.status(409).body(new ApiError(AlreadyOrderSomeProductsException.ERROR_VALUE));

        } catch (ObjectOptimisticLockingFailureException exception) {

            LOGGER.warn("Concurrent change", exception);

            return ResponseEntity.status(409).body(ApiError.staleUserData());

        }
    }

}
