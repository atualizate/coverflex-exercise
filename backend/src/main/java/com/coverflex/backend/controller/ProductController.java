package com.coverflex.backend.controller;

import com.coverflex.backend.domain.Product;
import com.coverflex.backend.domain.dto.ProductsDTO;
import com.coverflex.backend.domain.dto.ProductsDTO.ProductDTO;
import com.coverflex.backend.framework.dto.DTO;
import com.coverflex.backend.repository.ProductsRepository;
import com.coverflex.backend.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    private final static Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private ProductsRepository repository;

    @GetMapping(value = "/products")
    DTO listAllProducts() {

        LOGGER.info("Returning items");

        List<ProductDTO> productDTOList = this.repository.findAll()
                .stream()
                .map(Product::toDTO)
                .collect(Collectors.toList());

        return new ProductsDTO(productDTOList);
    }

}
