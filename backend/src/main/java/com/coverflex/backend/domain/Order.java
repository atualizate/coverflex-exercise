package com.coverflex.backend.domain;

import com.coverflex.backend.domain.dto.OrderDTO;
import com.coverflex.backend.framework.domain.Money;
import com.coverflex.backend.framework.dto.DTO;
import com.coverflex.backend.framework.dto.DTOable;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


@Entity
@Table(name = "ORDERS")
public class Order implements DTOable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductOrder> orderedProducts = new HashSet<>();

    @Embedded
    private Money total = new Money(0);

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public Order() {
    }

    public void addProductOrder(ProductOrder... productOrders) {
        for (ProductOrder productOrder : productOrders) {
            if (this != productOrder.getOrder()) {
                productOrder.setOrder(this);
            }
            this.orderedProducts.add(productOrder);
        }

        this.total = this.orderedProducts
                .stream()
                .map(ProductOrder::getPrice)
                .reduce(new Money(0), Money::add);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<ProductOrder> getOrderedProducts() {
        return orderedProducts;
    }

    public Money total() {
        return total;
    }

    public void setTotal(Money total) {
        this.total = total;
    }

    public Date getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", user=" + user +
                ", orderedProducts=" + orderedProducts +
                ", total=" + total +
                ", created=" + created +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return user.equals(order.user) && orderedProducts.equals(order.orderedProducts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, orderedProducts);
    }

    @Override
    public DTO toDTO() {
        return new OrderDTO(
                this.id.toString(),
                this.total.amountAsFloat(),
                this.getOrderedProducts().stream().map(ProductOrder::getProductId).collect(Collectors.toSet())
        );
    }
}
