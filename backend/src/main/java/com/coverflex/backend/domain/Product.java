package com.coverflex.backend.domain;

import com.coverflex.backend.domain.dto.ProductsDTO;
import com.coverflex.backend.framework.domain.Money;
import com.coverflex.backend.framework.dto.DTOable;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;


@Entity
public class Product implements DTOable {

    @Id
    private String id;

    @Column(nullable = false)
    private String name;

    @Embedded
    private Money price;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    protected Product() {
    }

    public Product(String id, String name, float price) {
        this.id = id;
        this.name = name;
        this.price = new Money(price);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price.setAmount(price);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product products = (Product) o;
        return getId().equals(products.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }


    @Override
    public ProductsDTO.ProductDTO toDTO() {
        return new ProductsDTO.ProductDTO(this.id, this.name, this.price.amountAsFloat());
    }
}
