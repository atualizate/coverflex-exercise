package com.coverflex.backend.domain;

import com.coverflex.backend.framework.domain.Money;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"USER_ID", "PRODUCT_ID"}))
public class ProductOrder {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "product_id", nullable = false, updatable = false)
    private String productId;

    @Column(nullable = false, updatable = false)
    private String name;

    @Embedded
    private Money price;

    @Column(nullable = false, name = "user_id")
    private String userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    public ProductOrder() {
    }

    public ProductOrder(Product product, User user) {
        this.productId = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.userId = user.getId();
    }

    public Long getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public Money getPrice() {
        return price;
    }

    public String getUserId() {
        return this.userId;
    }

    public Order getOrder() {
        return order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductOrder)) return false;
        ProductOrder that = (ProductOrder) o;
        return getProductId().equals(that.getProductId()) && this.userId.equals(that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductId(), userId);
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
