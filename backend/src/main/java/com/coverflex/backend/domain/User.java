package com.coverflex.backend.domain;

import com.coverflex.backend.domain.exception.InsufficientFundsException;
import com.coverflex.backend.framework.domain.Money;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.*;


@Entity
public class User {

    public static final int DEFAULT_BALANCE = 500;

    @Id
    private String id;

    @Embedded
    private Money balance;

    @Version
    private long version;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Order> orders = new ArrayList<>();

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    protected User() {
    }

    public User(String id) {
        this(id, DEFAULT_BALANCE);
    }

    public User(String id, int balance) {
        this(id, balance, new ArrayList<>());
    }

    public User(String id, int balance, List<Order> orders) {
        this.id = id;
        this.balance = new Money(balance);
        orders.forEach(this::addOrder);
    }

    private void addOrder(Order... orders) {
        for (Order order : orders) {
            if (this != order.getUser()) {
                order.setUser(this);
            }
            this.orders.add(order);
        }
    }

    public void setBalance(Money balance) {
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public Money getBalance() {
        return balance;
    }

    /**
     * Place order
     *
     * @param products
     * @return success
     * @throws InsufficientFundsException if total is greater than balance
     */
    public Order createOrderForProducts(Set<Product> products) {

        Order order = new Order();

        for (Product product : products) {
            order.addProductOrder(new ProductOrder(product, this));
        }

        this.subtractFromBalance(order.total());
        this.addOrder(order);

        return order;
    }

    /**
     * Charge amount
     *
     * @param total
     * @throws InsufficientFundsException if total is greater than balance
     */
    private void subtractFromBalance(Money total) {
        final Money newBalance = this.balance.subtract(total);

        if (newBalance.negativeAmount()) {
            throw new InsufficientFundsException(
                    String.format(
                            "User %s not has sufficient funds to charge %s. Current balance: %s",
                            this.id,
                            total,
                            this.balance.toString()
                    )
            );
        }

        this.balance = newBalance;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + id + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId().equals(user.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}
