package com.coverflex.backend.domain.dto;

import com.coverflex.backend.domain.User;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class AccountDetailsDTO {

    private UserDTO user;

    protected AccountDetailsDTO() {
    }

    public AccountDetailsDTO(User user, Set<String> productIds) {
        this(user.getId(), user.getBalance().amountAsFloat(), productIds);
    }

    public AccountDetailsDTO(String userId, float balance, Set<String> productsIds) {
        this.user = new UserDTO(userId, new DetailsDTO(balance, productsIds));
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public static class UserDTO {
        @JsonProperty("user_id")
        private String userId;

        private DetailsDTO data;

        protected UserDTO() {
        }

        public UserDTO(String userId, DetailsDTO data) {
            this.userId = userId;
            this.data = data;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public DetailsDTO getData() {
            return data;
        }

        public void setData(DetailsDTO data) {
            this.data = data;
        }
    }

    public static class DetailsDTO {
        private float balance;
        @JsonProperty("product_ids")
        private Set<String> productIds;

        protected DetailsDTO() {
        }

        public DetailsDTO(float balance, Set<String> productIds) {
            this.balance = balance;
            this.productIds = productIds;
        }

        public double getBalance() {
            return balance;
        }

        public void setBalance(float balance) {
            this.balance = balance;
        }

        public Set<String> getProductIds() {
            return productIds;
        }

        public void setProductIds(Set<String> productIds) {
            this.productIds = productIds;
        }
    }
}
