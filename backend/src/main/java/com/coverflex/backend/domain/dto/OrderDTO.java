package com.coverflex.backend.domain.dto;

import com.coverflex.backend.framework.dto.DTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class OrderDTO implements DTO {

    private OrderDetails order;

    public OrderDTO(String orderId, float total, Set<String> items) {
        this.order = new OrderDetails(orderId, total, items);
    }

    public OrderDetails getOrder() {
        return order;
    }

    public void setOrder(OrderDetails order) {
        this.order = order;
    }

    public static class ItemsOrder {

        private float total;

        private Set<String> items;

        public ItemsOrder(float total, Set<String> items) {
            this.total = total;
            this.items = items;
        }

        public float getTotal() {
            return total;
        }

        public void setTotal(float total) {
            this.total = total;
        }

        public Set<String> getItems() {
            return items;
        }

        public void setItems(Set<String> items) {
            this.items = items;
        }
    }

    public static class OrderDetails {

        @JsonProperty("order_id")
        private String orderId;

        private ItemsOrder data;

        public OrderDetails(String orderId, float total, Set<String> items) {
            this.orderId = orderId;
            this.data = new ItemsOrder(total, items);
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public ItemsOrder getData() {
            return data;
        }

        public void setData(ItemsOrder data) {
            this.data = data;
        }
    }

}
