package com.coverflex.backend.domain.dto;

import com.coverflex.backend.framework.dto.DTO;

import java.util.List;

public class ProductsDTO implements DTO {

    private List<ProductDTO>  products;

    public ProductsDTO(List<ProductDTO> products) {
        this.products = products;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public static class ProductDTO implements DTO {

        private String id;

        private String name;

        private float price;

        public ProductDTO(String id, String name, float price) {
            this.id = id;
            this.name = name;
            this.price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }
    }

}
