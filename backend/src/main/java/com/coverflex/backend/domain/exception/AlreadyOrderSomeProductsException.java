package com.coverflex.backend.domain.exception;

public class AlreadyOrderSomeProductsException extends RuntimeException {
    public final static String ERROR_VALUE = "products_already_purchased";
}
