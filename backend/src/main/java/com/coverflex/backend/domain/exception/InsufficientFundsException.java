package com.coverflex.backend.domain.exception;

public class InsufficientFundsException extends RuntimeException {

    public final static String ERROR_VALUE = "insufficient_balance";

    public InsufficientFundsException(String message) {
        super(message);
    }
}
