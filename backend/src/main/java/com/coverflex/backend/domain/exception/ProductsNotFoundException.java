package com.coverflex.backend.domain.exception;

public class ProductsNotFoundException extends RuntimeException {

    public final static String ERROR_VALUE = "products_not_found";

    public ProductsNotFoundException(String message) {
        super(message);
    }
}
