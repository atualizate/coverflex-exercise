package com.coverflex.backend.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

public class OrderRequest {

    @Valid
    private OrderDetails order;

    public OrderRequest() {}

    public OrderRequest(String userId, Set<String> productsIds) {
        this.order = new OrderDetails(userId, productsIds);
    }

    public OrderDetails getOrder() {
        return order;
    }

    public void setOrder(OrderDetails order) {
        this.order = order;
    }

    public Set<String> getItems() {
        return this.order.getItems();
    }

    public String getUserId() {
        return this.order.getUserId();
    }

    public static class OrderDetails {

        @Valid
        @NotBlank
        @Size(max = 20)
        @Pattern(regexp = "^[aA-zZ0-9]+$")
        @JsonProperty(value = "user_id")
        private String userId;

        @Valid
        @NotEmpty(message = "At least one product must be present")
        private Set<String> items;

        public OrderDetails() {
        }

        public OrderDetails(String userId, Set<String> items) {
            this.userId = userId;
            this.items = items;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public Set<String> getItems() {
            return items;
        }

        public void setItems(Set<String> items) {
            this.items = items;
        }

        @Override
        public String toString() {
            return "OrderDetails{" +
                    "userId='" + userId + '\'' +
                    ", items=" + items +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "OrderRequest{" +
                "order=" + order +
                '}';
    }
}
