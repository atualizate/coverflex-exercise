package com.coverflex.backend.framework.api;

public class ApiError {


    private String error;

    public ApiError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static ApiError staleUserData() {
        return new ApiError("stale_user_data");
    }
}
