package com.coverflex.backend.framework.domain;

import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

@Embeddable
public class Money {

    private static final RoundingMode DEFAULT_ROUNDING = RoundingMode.HALF_EVEN;

    private BigDecimal amount;
    private Currency currency;

    protected Money() {
    }

    public Money(float amount) {
        this(new BigDecimal(amount), Currency.getInstance(Locale.getDefault()));
    }

    public Money(BigDecimal amount) {
        this(amount, Currency.getInstance(Locale.getDefault()));
    }

    public Money(BigDecimal amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public float amountAsFloat() {
        return this.amount.setScale(2, DEFAULT_ROUNDING).floatValue();
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Money subtract(Money money) {
        return new Money(this.amount.subtract(money.amount));
    }

    public Money add(Money money) {
        return new Money(this.amount.add(money.amount));
    }


    public boolean negativeAmount() {
        return this.amount.compareTo(new BigDecimal(0)) < 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Money)) return false;
        Money money = (Money) o;
        return getAmount().equals(money.getAmount()) && getCurrency().equals(money.getCurrency());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAmount(), getCurrency());
    }
}
