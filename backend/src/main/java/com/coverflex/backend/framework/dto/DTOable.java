package com.coverflex.backend.framework.dto;

public interface DTOable {
    DTO toDTO();
}
