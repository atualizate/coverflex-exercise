package com.coverflex.backend.repository;

import com.coverflex.backend.domain.Order;
import com.coverflex.backend.domain.ProductOrder;
import com.coverflex.backend.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query("SELECT o.orderedProducts FROM Order o WHERE o.user = ?1")
    List<ProductOrder> getOrderedProductsByUser(User user);

    @Query("SELECT count(p) FROM Order o JOIN o.orderedProducts p WHERE o.user = ?1 AND p.productId IN (?2)")
    long numberOfAlreadyOrderedProducts(User user, Collection<String> productIds);

}
