package com.coverflex.backend.repository;

import com.coverflex.backend.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductsRepository extends JpaRepository<Product, String> {
}
