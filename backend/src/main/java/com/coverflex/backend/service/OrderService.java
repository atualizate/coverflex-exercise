package com.coverflex.backend.service;

import com.coverflex.backend.domain.Order;
import com.coverflex.backend.domain.Product;
import com.coverflex.backend.domain.User;
import com.coverflex.backend.domain.exception.AlreadyOrderSomeProductsException;
import com.coverflex.backend.domain.exception.ProductsNotFoundException;
import com.coverflex.backend.repository.OrderRepository;
import com.coverflex.backend.repository.ProductsRepository;
import com.coverflex.backend.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductsRepository productsRepository;

    /**
     * Obtain products by id
     *
     * @param productIds set of products to find
     * @return List of products
     * @throws ProductsNotFoundException if the returned products from database does not match with the length of the argument set size
     */
    public Set<Product> getProductsByIds(Set<String> productIds) {

        final Set<Product> products = new HashSet<>(productsRepository.findAllById(productIds));

        if (products.size() != productIds.size()) {
            throw new ProductsNotFoundException("Some products were not found");
        }

        return products;
    }

    /**
     * Check if products are not already ordered
     *
     * @param user
     * @param products
     * @return always true or exception
     * @throws AlreadyOrderSomeProductsException if user already ordered some products
     */
    public boolean validateIfAreProductsAlreadyOrdered(User user, Set<Product> products) {

        long orderedProductsCounter = orderRepository.numberOfAlreadyOrderedProducts(
                user,
                products.stream().map(Product::getId).collect(Collectors.toList())
        );

        if (orderedProductsCounter > 0) {
            throw new AlreadyOrderSomeProductsException();
        }

        return true;
    }

    /**
     * Validate and create order
     *
     * @param userId     user identifier
     * @param productIds set of products to be ordered
     * @return order with products
     * @throws AlreadyOrderSomeProductsException                                 if user already ordered some products
     * @throws ProductsNotFoundException                                         if the returned products from database does not match with the length of the argument set size
     * @throws com.coverflex.backend.domain.exception.InsufficientFundsException if user not has sufficient balance for the order
     */
    @Transactional(rollbackFor = {DataIntegrityViolationException.class, ObjectOptimisticLockingFailureException.class})
    public Order processOrder(String userId, Set<String> productIds) {

        final User user = this.userRepository.findById(userId)
                .orElseGet(() -> userRepository.save(new User(userId)));

        final Set<Product> products = this.getProductsByIds(productIds);

        this.validateIfAreProductsAlreadyOrdered(user, products);

        Order order = user.createOrderForProducts(products);

        return this.save(user, order);
    }

    protected Order save(User user, Order order) {
        final Order managedOrder = this.orderRepository.save(order);
        this.userRepository.save(user);
        return managedOrder;
    }

}
