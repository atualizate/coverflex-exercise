package com.coverflex.backend.controller;

import com.coverflex.backend.domain.Product;
import com.coverflex.backend.domain.User;
import com.coverflex.backend.domain.dto.AccountDetailsDTO;
import com.coverflex.backend.repository.OrderRepository;
import com.coverflex.backend.repository.ProductsRepository;
import com.coverflex.backend.repository.UserRepository;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductsRepository productsRepository;

    @Test
    public void shouldReturnWrongMediaTypeWhenClientNotAcceptsJSON() throws Exception {
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/users/test").accept("application/xml"))
                .andExpect(status().is(406));
    }

    @Test
    public void shouldReturnAccountDetailsWithAnEmptyItemsList() throws Exception {
        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.get("/users/details"))
                .andExpect(status().is(200))
                .andExpect(content().string(matchesJsonSchemaInClasspath("schemas/account.json")))
                .andReturn();

        final String contentAsString = mvcResult.getResponse().getContentAsString();

        final AccountDetailsDTO accountDetailsDTO = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
                .fromJson(contentAsString, AccountDetailsDTO.class);

        assertEquals("details", accountDetailsDTO.getUser().getUserId());
        assertEquals(User.DEFAULT_BALANCE, accountDetailsDTO.getUser().getData().getBalance());
        assertTrue(accountDetailsDTO.getUser().getData().getProductIds().isEmpty(), "Empty list of products");
    }

    @Test
    public void shouldReturnAccountDetailsWithAnNonEmptyItemsList() throws Exception {

        final String username = "details1";

        final User user = this.userRepository.saveAndFlush(new User(username));

        final List<Product> productList = productsRepository.findAllById(Arrays.asList("twitch", "zulu"));

        assertEquals(2, productList.size(), "Must encounter 2 products");

        user.createOrderForProducts(new HashSet<>(productList));

        userRepository.saveAndFlush(user);

        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.get("/users/" + username))
                .andExpect(status().is(200))
                .andExpect(content().string(matchesJsonSchemaInClasspath("schemas/account.json")))
                .andReturn();

        final String contentAsString = mvcResult.getResponse().getContentAsString();

        final AccountDetailsDTO accountDetailsDTO = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
                .fromJson(contentAsString, AccountDetailsDTO.class);

        assertEquals(username, accountDetailsDTO.getUser().getUserId());
        assertEquals((User.DEFAULT_BALANCE - 500), accountDetailsDTO.getUser().getData().getBalance());
        assertTrue(accountDetailsDTO.getUser().getData().getProductIds().contains("zulu"), "Must contain zulu");
        assertTrue(accountDetailsDTO.getUser().getData().getProductIds().contains("twitch"), "Must contain twitch");
    }


}
