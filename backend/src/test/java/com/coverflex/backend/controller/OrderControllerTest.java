package com.coverflex.backend.controller;

import com.coverflex.backend.domain.Order;
import com.coverflex.backend.domain.Product;
import com.coverflex.backend.domain.ProductOrder;
import com.coverflex.backend.domain.User;
import com.coverflex.backend.domain.dto.OrderDTO;
import com.coverflex.backend.domain.exception.AlreadyOrderSomeProductsException;
import com.coverflex.backend.domain.exception.InsufficientFundsException;
import com.coverflex.backend.domain.exception.ProductsNotFoundException;
import com.coverflex.backend.domain.request.OrderRequest;
import com.coverflex.backend.framework.api.ApiError;
import com.coverflex.backend.repository.OrderRepository;
import com.coverflex.backend.repository.ProductsRepository;
import com.coverflex.backend.repository.UserRepository;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private OrderRepository orderRepository;


    @Test
    public void shouldCreateOrder() throws Exception {

        final Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        final OrderRequest orderRequest = new OrderRequest("test10", new HashSet<>(Arrays.asList("zulu", "youtube")));

        final String payload = gson.toJson(orderRequest);

        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.post("/orders").content(payload).contentType("application/json"))
                .andExpect(status().is(200))
                .andExpect(content().string(matchesJsonSchemaInClasspath("schemas/order.json")))
                .andReturn();

        final OrderDTO orderDTO = gson.fromJson(mvcResult.getResponse().getContentAsString(), OrderDTO.class);

        assertNotNull(orderDTO.getOrder().getOrderId(), "Must be some string");
        assertTrue(orderDTO.getOrder().getOrderId().length() > 0, "Must not be empty");
        assertEquals(2, orderDTO.getOrder().getData().getItems().size(), "Must have two items");
        assertEquals(250.0, orderDTO.getOrder().getData().getTotal(), "total must be 250");
    }

    @Test
    public void shouldSendBadRequestWithAlreadyBuyValue() throws Exception {

        final User user = this.userRepository.save(
                new User("test11")
        );

        final List<Product> productList = productsRepository.findAllById(Collections.singletonList("twitch"));
        assertEquals(1, productList.size());

        final Order order1 = new Order();
        order1.addProductOrder(new ProductOrder(productList.get(0), user));
        order1.setUser(user);

        orderRepository.save(order1);

        final Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        final OrderRequest orderRequest = new OrderRequest("test11", new HashSet<>(Arrays.asList("zulu", "twitch")));

        final String payload = gson.toJson(orderRequest);

        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.post("/orders").content(payload).contentType("application/json"))
                .andExpect(status().is(400))
                .andReturn();

        final ApiError error = gson.fromJson(mvcResult.getResponse().getContentAsString(), ApiError.class);

        assertEquals(AlreadyOrderSomeProductsException.ERROR_VALUE, error.getError(), "Must have the same value");
    }


    @Test
    public void shouldSendBadRequestWithNotFoundValue() throws Exception {

        final Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        final OrderRequest orderRequest = new OrderRequest("test12", new HashSet<>(Arrays.asList("nnotaservice", "twitch")));

        final String payload = gson.toJson(orderRequest);

        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.post("/orders").content(payload).contentType("application/json"))
                .andExpect(status().is(400))
                .andReturn();

        final ApiError error = gson.fromJson(mvcResult.getResponse().getContentAsString(), ApiError.class);

        assertEquals(ProductsNotFoundException.ERROR_VALUE, error.getError(), "Must have the same value");
    }

    @Test
    public void shouldSendBadRequestWithNoBalanceValue() throws Exception {

        final Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        final OrderRequest orderRequest = new OrderRequest("test12", new HashSet<>(Arrays.asList("disney", "twitch")));

        final String payload = gson.toJson(orderRequest);

        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.post("/orders").content(payload).contentType("application/json"))
                .andExpect(status().is(400))
                .andReturn();

        final ApiError error = gson.fromJson(mvcResult.getResponse().getContentAsString(), ApiError.class);

        assertEquals(InsufficientFundsException.ERROR_VALUE, error.getError(), "Must have the same value");
    }


}
