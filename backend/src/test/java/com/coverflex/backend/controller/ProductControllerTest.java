package com.coverflex.backend.controller;

import com.coverflex.backend.domain.dto.ProductsDTO;
import com.coverflex.backend.repository.ProductsRepository;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ProductsRepository productsRepository;


    @Test
    public void shouldReturnListOfProducts() throws Exception {
        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.get("/products"))
                .andExpect(status().is(200))
                .andExpect(content().string(matchesJsonSchemaInClasspath("schemas/products.json")))
                .andReturn();

        final String contentAsString = mvcResult.getResponse().getContentAsString();

        final ProductsDTO productsDTO = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
                .fromJson(contentAsString, ProductsDTO.class);

        assertEquals(productsRepository.count(), productsDTO.getProducts().size(), "Same size");
    }

}
