package com.coverflex.backend.domain;

import com.coverflex.backend.domain.exception.InsufficientFundsException;
import com.coverflex.backend.framework.domain.Money;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserTest {

    @Test
    void shouldReturnDefaultBalance() {

        final Money expected = new Money(500);
        final User user = new User("test", 500);

        assertEquals(expected, user.getBalance(), "Same balance");

    }

    @Test
    void shouldPerformBalanceArithmetic() {

        final Product zulu = new Product("zulu", "Zulu", 150);
        final Product youtube = new Product("youtube", "YouTube", 100);

        final Money expectedOnFirstOrder = new Money((500 - 250));

        final User user = new User("test", 500);

        user.createOrderForProducts(new HashSet<>(Arrays.asList(zulu, youtube)));

        assertEquals(expectedOnFirstOrder, user.getBalance(), "Must have 250 amount as balance");

        final Money expectedOnSecondOrder = new Money(0);

        user.createOrderForProducts(new HashSet<>(Arrays.asList(zulu, youtube)));

        assertEquals(expectedOnSecondOrder, user.getBalance(), "Must have 0 amount as balance");

    }

    @Test
    void shouldThrowExceptionWhenOrderTotalIsGreaterThanBalance() {

        final Product zulu = new Product("zulu", "Zulu", 499);
        final Product youtube = new Product("youtube", "YouTube", 2);

        final User user = new User("test", 500);

        assertThrows(InsufficientFundsException.class, () -> {
            user.createOrderForProducts(new HashSet<>(Arrays.asList(zulu, youtube)));
        }, "Not allow 501 total when balance is 500 amount");

    }


}
