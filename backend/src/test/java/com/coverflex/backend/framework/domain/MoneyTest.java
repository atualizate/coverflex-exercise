package com.coverflex.backend.framework.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoneyTest {

    @Test
    void negativeAmountCentsTest() {

        final Money noBalance = new Money(0);
        assertFalse(noBalance.negativeAmount(), "0 is not negative");

        final Money oneCent = new Money(0.01f);

        final Money result = noBalance.subtract(oneCent);

        assertTrue(result.negativeAmount(), "must be negative");

    }
}
