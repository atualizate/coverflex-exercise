package com.coverflex.backend.service;

import com.coverflex.backend.domain.Order;
import com.coverflex.backend.domain.Product;
import com.coverflex.backend.domain.ProductOrder;
import com.coverflex.backend.domain.User;
import com.coverflex.backend.domain.exception.AlreadyOrderSomeProductsException;
import com.coverflex.backend.domain.exception.ProductsNotFoundException;
import com.coverflex.backend.framework.domain.Money;
import com.coverflex.backend.repository.OrderRepository;
import com.coverflex.backend.repository.ProductsRepository;
import com.coverflex.backend.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private OrderRepository orderRepository;


    @Test
    public void shouldThrowExceptionForMissingProducts() {
        assertThrows(ProductsNotFoundException.class, () -> {
            orderService.getProductsByIds(new HashSet<>(Arrays.asList("test", "nothing")));
        }, "Test and nothing not exists");
    }

    @Test
    public void shouldThrowExceptionForPartialMissingProducts() {
        assertThrows(ProductsNotFoundException.class, () -> {
            orderService.getProductsByIds(new HashSet<>(Arrays.asList("zulu", "nothing")));
        }, "Zulu exists but and nothing not exists");
    }

    @Test
    public void shouldReturnAListOfProducts() {
        final Product zulu = new Product("zulu", "Zulu", 150);
        final Product youtube = new Product("youtube", "YouTube", 100);

        final Set<Product> productsByIds = orderService.getProductsByIds(new HashSet<>(Arrays.asList("zulu", "youtube")));

        assertEquals(2, productsByIds.size(), "Must have 2 items");
        assertTrue(productsByIds.contains(zulu), "Must have zulu service");
        assertTrue(productsByIds.contains(youtube), "Must have youtube service");
    }

    @Test
    public void shouldThrowExceptionWhenUserOrderSameProducts() {

        final Set<Product> productsByIds = orderService.getProductsByIds(new HashSet<>(Arrays.asList("zulu", "youtube")));

        final User user = new User("test500", 500);
        user.createOrderForProducts(productsByIds);

        final User managedUser = this.userRepository.save(user);

        assertThrows(AlreadyOrderSomeProductsException.class,
                () -> {
                    orderService.validateIfAreProductsAlreadyOrdered(managedUser, productsByIds);
                }, "User cannot order same products");
    }

    @Test
    public void shouldProductsBeValidForNewOrder() {

        final Set<Product> productsByIds = orderService.getProductsByIds(new HashSet<>(Arrays.asList("zulu", "youtube")));

        final User user = this.userRepository.save(
                new User("test2")
        );

        assertTrue(orderService.validateIfAreProductsAlreadyOrdered(user, productsByIds), "Must be true since user does not have any order");

        this.userRepository.delete(user);
    }

    @Test
    public void shouldThrowUniqueExceptionForDuplicatedItemsWhenTryToCreateOrder() {

        final User user = this.userRepository.save(
                new User("test300")
        );

        final List<Product> productList = productsRepository.findAllById(Collections.singletonList("twitch"));

        final Order order1 = new Order();
        order1.addProductOrder(new ProductOrder(productList.get(0), user));
        order1.setUser(user);

        final Order order2 = new Order();
        order2.addProductOrder(new ProductOrder(productList.get(0), user));
        order2.setUser(user);

        orderRepository.save(order1);

        assertThrows(DataIntegrityViolationException.class, () -> {
            orderRepository.save(order2);
        }, "Unique index pair must not be violated");

    }

    @Test
    public void shouldThrowOptimisticLockingErrorWhenChangingDifferentUsers() {

        final User user = this.userRepository.save(
                new User("test4")
        );

        user.setBalance(new Money(600));
        userRepository.save(user);

        assertEquals(new Money(600), user.getBalance(), "Same balance");

        user.setBalance(new Money(700));

        assertThrows(ObjectOptimisticLockingFailureException.class, () -> {
            userRepository.save(user);
        }, "Cannot change different user versions");

    }

    @Test
    public void shouldCreateOrder() {

        final Order order = this.orderService.processOrder("test100", new HashSet<>(Arrays.asList("zulu", "youtube")));

        assertTrue(order.getId() != 0, "Must have an database id");

        assertEquals(2, order.getOrderedProducts().size(), "Same number of products");
    }


}
