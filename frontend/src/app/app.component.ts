import {Component} from '@angular/core';
import {AccountService} from "../services/account.service";
import {animate, style, transition, trigger} from "@angular/animations";
import {RouterOutlet} from "@angular/router";

const slideInAnimation = trigger('routeAnimations', [
  transition('*<=>*', [
    style({opacity: 0}),
    animate('0.4s', style({opacity: 1}))
  ])
]);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    slideInAnimation
  ]
})
export class AppComponent {
  title = 'conver';

  hasUser: boolean;
  currentBalance: number;

  constructor(private accountService: AccountService) {
    this.hasUser = false;
    this.currentBalance = 0;

    this.accountService
      .user
      .subscribe((maybeUser) => {
        this.hasUser = !!maybeUser;
      });

    this.accountService
      .balance
      .subscribe((currentBalance) => {
        this.currentBalance = currentBalance;
      });
  }

  prepareRoute(outlet: RouterOutlet): boolean {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
