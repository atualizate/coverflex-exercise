import {DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HomeComponent} from './home/home.component';
import {appRoutingModule} from "./app.routing";
import {TopNavigationComponent} from './top-navigation/top-navigation.component';
import {FlexLayoutModule, FlexModule} from "@angular/flex-layout";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from "@angular/material/input";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatButtonModule} from "@angular/material/button";
import {ProductComponent} from './products/product/product.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ProductsComponent} from "./products/products.component";
import {MatMenuModule} from "@angular/material/menu";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatIconModule} from "@angular/material/icon";
import {MatBadgeModule} from "@angular/material/badge";
import {CartComponent} from './cart/cart.component';
import {HistoricOrdersComponent} from './historic-orders/historic-orders.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {CorrelationIdInterceptor} from "../helpers/correlation-id.interceptor";
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    TopNavigationComponent,
    ProductComponent,
    ProductsComponent,
    CartComponent,
    HistoricOrdersComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    appRoutingModule,
    FlexModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    HttpClientModule,
    FlexLayoutModule,
    MatMenuModule,
    MatTooltipModule,
    MatIconModule,
    MatBadgeModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
  ],
  providers: [
    {
      provide: DEFAULT_CURRENCY_CODE,
      useValue: 'EUR'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CorrelationIdInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
