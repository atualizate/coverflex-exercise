import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {ProductsComponent} from "./products/products.component";
import {AuthGuard} from "../helpers/auth.guard";
import {CartComponent} from "./cart/cart.component";
import {HistoricOrdersComponent} from "./historic-orders/historic-orders.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'products', component: ProductsComponent, canActivate: [AuthGuard]},
  {path: 'cart', component: CartComponent, canActivate: [AuthGuard]},
  {path: 'orders/historic', component: HistoricOrdersComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: '/'}
]

export const appRoutingModule = RouterModule.forRoot(routes);
