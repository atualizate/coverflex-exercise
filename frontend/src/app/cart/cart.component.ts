import {Component, OnDestroy} from '@angular/core';
import {CartService} from "../../services/cart.service";
import {Product} from "../../models/product";
import {OrdersService} from "../../services/orders.service";
import {HttpErrorResponse} from "@angular/common/http";
import {FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {switchMap} from "rxjs/operators";
import {throwError} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnDestroy {

  isLoading: boolean;

  cartResetRequired: boolean;

  cartForm: FormGroup;

  constructor(
    private router: Router,
    private accountService: AccountService,
    private orderService: OrdersService,
    private title: Title,
    private cartService: CartService) {
    this.title.setTitle("Cart");
    this.isLoading = false;
    this.cartResetRequired = false;
    this.cartForm = new FormGroup({});
  }

  ngOnDestroy(): void {
    if (this.cartResetRequired) {
      this.cartService.resetCart();
    }
  }

  items(): Array<Product> {
    return this.cartService.products();
  }

  isEmpty(): boolean {
    return this.cartService.numberOfItems === 0;
  }

  cost(): number {
    return this.cartService.total;
  }

  submitCart(): void {

    this.isLoading = true;

    this.accountService
      .user
      .pipe(
        switchMap((user) => {
          if (user !== null) {
            return this.orderService.placeOrder(this.cartService.newOrder(user))
          } else {
            return throwError(new Error("User not found"));
          }
        }))
      .subscribe(() => {

          this.isLoading = false;

          this.accountService.subtractFromBalance(this.cost())

          this.cartService.resetCart();

          this.router.navigate(['/orders/historic']).finally();

        }, (error) => {

          this.isLoading = false;

          if (error instanceof HttpErrorResponse) {
            switch (error.status) {
              case 400:
              case 409:

                if (error.error
                  && error.error.hasOwnProperty('error')
                  && typeof error.error.error === 'string') {

                  const errorKey = error.error.error;

                  if (errorKey !== 'products_not_found') {
                    // We have stale information
                    this.accountService.reloadAccountDetails();
                  } else {
                    // We don't want to delete all items after submit
                    // since the backend does not reply with the products
                    // that were not found.
                    this.cartResetRequired = true;
                  }

                  this.cartForm.setErrors({
                    [errorKey]: true
                  });
                }
                return;
            }

            this.cartForm.setErrors({
              request: true
            });
            return;
          }

          this.cartForm.setErrors({
            general: true
          });

          console.log(error);
        }
      );
  }
}
