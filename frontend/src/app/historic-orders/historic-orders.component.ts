import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {OrdersService} from "../../services/orders.service";
import {Product} from "../../models/product";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-historic-orders',
  templateUrl: './historic-orders.component.html',
  styleUrls: ['./historic-orders.component.css']
})
export class HistoricOrdersComponent implements OnInit {

  isLoading: boolean;

  orderedProducts: Array<Product>;

  constructor(private apiService: ApiService,
              private snackBar: MatSnackBar,
              private title: Title,
              private orderService: OrdersService) {
    this.title.setTitle("Historic");
    this.isLoading = false;
    this.orderedProducts = [];
  }

  ngOnInit(): void {
    this.isLoading = true;

    this.apiService
      .fetchProducts()
      .subscribe((products) => {
        this.isLoading = false;
        this.orderedProducts = products.filter(p => this.orderService.hasProductId(p));
      }, (error) => {
        console.log(error);
        this.snackBar.open("Not able to loads the products. Check your internet", 'Ok', {
          duration: 3000
        });
      });
  }

}
