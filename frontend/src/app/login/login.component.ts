import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm: FormGroup;

  isLoading: boolean;
  returnUrl: string;

  constructor(private router: Router,
              private title: Title,
              private activatedRoute: ActivatedRoute,
              private accountService: AccountService) {

    this.title.setTitle('Login');

    this.isLoading = false;
    this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
    this.loginForm = new FormGroup({
      username: new FormControl('',
        [
          Validators.required,
          Validators.pattern(/^[aA-zZ]+$/)
        ])
    });
  }

  submitLogin(): void {
    this.isLoading = true;

    const username = this.loginForm.getRawValue().username;

    this.accountService.login(username)
      .subscribe((success) => {
        this.isLoading = false;
        if (success) {
          this.router.navigate([this.returnUrl]).finally();
        } else {

        }
      }, (error) => {
        this.isLoading = false;
        this.loginForm.setErrors({
          general: true
        });

        console.log(error);
      });
  }
}
