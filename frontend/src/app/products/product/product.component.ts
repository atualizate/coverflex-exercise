import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from "../../../models/product";

export enum SelectionAction {
  REMOVE, ADD
}

export interface ProductSelectionEvent {
  product: Product;
  action: SelectionAction;
}

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {

  @Input()
  showIndicators: boolean;

  @Input()
  alreadyOrdered: boolean;

  @Input()
  selected: boolean;

  @Input()
  disabled: boolean;

  @Input()
  product!: Product;

  @Output()
  change: EventEmitter<ProductSelectionEvent>;

  constructor() {
    this.selected = false;
    this.disabled = false;
    this.alreadyOrdered = false;
    this.showIndicators = false;
    this.change = new EventEmitter<ProductSelectionEvent>();
  }

  onSelected(selected: boolean) {
    this.change.emit(
      {
        product: this.product,
        action: selected ? SelectionAction.ADD : SelectionAction.REMOVE
      });
  }

  isDisabled(): boolean {
    return !this.alreadyOrdered && !this.selected && this.disabled;
  }
}
