import {Component, OnInit} from '@angular/core';
import {Product} from "../../models/product";
import {ApiService} from "../../services/api.service";
import {ProductSelectionEvent, SelectionAction} from "./product/product.component";
import {OrdersService} from "../../services/orders.service";
import {AccountService} from "../../services/account.service";
import {CartService} from "../../services/cart.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  isLoading: boolean;
  products: Array<Product>;

  constructor(
    private cartService: CartService,
    private accountService: AccountService,
    private orderService: OrdersService,
    private snackBar: MatSnackBar,
    private title: Title,
    private apiService: ApiService) {
    this.title.setTitle("Products");
    this.products = [];
    this.isLoading = false;
  }

  numberItemsOnBasket(): number {
    return this.cartService.numberOfItems;
  }

  ngOnInit(): void {
    this.loadProducts();
  }

  loadProducts(): void {
    this.isLoading = true;
    this.apiService
      .fetchProducts()
      .subscribe((products) => {
        this.isLoading = false;
        this.products = products;
      }, (error) => {
        this.isLoading = false;
        console.log(error);
        this.snackBar.open("Not able to loads the products. check your internet", 'Ok', {
          duration: 3000
        });
      });
  }

  total(): number {
    return this.cartService.total;
  }

  alreadyOrdered(product: Product): boolean {
    return this.orderService.hasProductId(product);
  }

  isSelected(product: Product): boolean {
    return this.cartService.hasProduct(product);
  }

  processSelectionEvent($event: ProductSelectionEvent) {
    if ($event.action === SelectionAction.ADD) {
      if (!this.orderService.hasProductId($event.product)) {
        this.cartService.addProduct($event.product);
      }
    } else {
      this.cartService.removeProduct($event.product);
    }
  }

  hasBudget(product: Product): boolean {
    return product.price <= (this.accountService.currentBalance - this.cartService.total);
  }
}
