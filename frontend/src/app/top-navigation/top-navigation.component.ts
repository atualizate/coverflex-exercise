import {Component, Input} from '@angular/core';
import {AccountService} from "../../services/account.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-top-navigation',
  templateUrl: './top-navigation.component.html',
  styleUrls: ['./top-navigation.component.css']
})
export class TopNavigationComponent {

  @Input()
  hasUser: boolean;

  @Input()
  balance: number;

  constructor(private accountService: AccountService,
              private router: Router) {
    this.hasUser = false;
    this.balance = 0;
  }

  logout(): void {
    this.accountService.logout();
    this.router.navigate(["/"]).finally();
  }
}
