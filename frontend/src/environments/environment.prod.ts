export const environment = {
  production: true,
  server: 'http://localhost:4000',
  pathPrefix: 'api',
  resources: {
    user: '/user',
    products: '/products',
    orders: '/orders'
  }
};
