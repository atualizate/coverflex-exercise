import {Product} from "./product";

export class Cart {
  items: Map<string, Product>;
  private _total: number;

  constructor() {
    this.items = new Map<string, Product>();
    this._total = 0;
  }

  get numberOfItems(): number {
    return this.items.size;
  }

  get total(): number {
    return this._total;
  }

  products(): Array<Product> {
    return Array.from(this.items.values());
  }

  hasProduct(product: Product): boolean {
    return this.items.has(product.id);
  }

  addProduct(product: Product): void {
    this.items.set(product.id, product);
    this._total += product.price;
  }

  removeProduct(product: Product): void {
    if (this.items.delete(product.id)) {
      this._total -= product.price;
    }
  }
}
