import {User} from "../user";
import {Account} from "../account";
import {Injectable} from "@angular/core";
import {Adapter} from "../../framework/adapter";

export class AccountDetails {
  user: User;
  account: Account;
  items: Set<string>;

  constructor(user: User, account: Account, items: string[] = []) {
    this.user = user;
    this.account = account;
    this.items = new Set<string>(items);
  }
}

@Injectable({
  providedIn: "root",
})
export class AccountDetailsAdapter implements Adapter<AccountDetails> {
  adapt(data: any): AccountDetails {
    const user: User = {
      username: data.user.user_id
    };

    const account: Account = {
      balance: data.user.data.balance
    }

    return new AccountDetails(user, account, data.user.data.product_ids);
  }
}

