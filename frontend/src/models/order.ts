export interface Order {
  orderId?: string;
  total?: string;
  userId?: string;
  items: Array<string>;
}
