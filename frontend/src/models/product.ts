import {Adapter} from "../framework/adapter";
import {Injectable} from "@angular/core";

export class Product {
  id: string;
  name: string;
  price: number;

  constructor(id: string, name: string, price: number) {
    this.id = id;
    this.name = name;
    this.price = price;
  }
}


@Injectable({
  providedIn: "root",
})
export class ProductsAdapter implements Adapter<Array<Product>> {
  adapt(item: any): Array<Product> {
    return item.products;
  }
}
