import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {User} from "../models/user";
import {ApiService} from "./api.service";
import {map} from "rxjs/operators";
import {OrdersService} from "./orders.service";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private userSubject: BehaviorSubject<User | null>;
  private balanceSubject: BehaviorSubject<number>;

  _user: Observable<User | null>;
  _balance: Observable<number>;

  constructor(
    private apiService: ApiService,
    private orderService: OrdersService) {

    this.userSubject = new BehaviorSubject<User | null>(null);
    this._user = this.userSubject.asObservable();

    this.balanceSubject = new BehaviorSubject<number>(0);
    this._balance = this.balanceSubject.asObservable();
  }

  get user(): Observable<User | null> {
    return this._user;
  }

  get balance(): Observable<number> {
    return this._balance;
  }

  get currentBalance(): number {
    return this.balanceSubject.value;
  }

  public get hasUser(): boolean {
    return this.userSubject.value !== null;
  }

  public subtractFromBalance(amount: number): void {
    this.balanceSubject.next(this.balanceSubject.value - amount);
  }

  login(username: String): Observable<boolean> {
    return this.apiService.fetchAccountDetails(username)
      .pipe(
        map(response => {
          this.userSubject.next(response.user);

          this.balanceSubject.next(response.account.balance);
          this.orderService.alreadyOrdered = response.items;

          return true;
        })
      );
  }

  reloadAccountDetails(): void {
    if (this.hasUser) {
      this.login(this.userSubject.value?.username || '')
        .toPromise()
        .catch((error) => {
	   console.log(error);
        });
    }
  }

  logout(): void {
    this.userSubject.next(null);
    this.balanceSubject.next(0);
  }
}
