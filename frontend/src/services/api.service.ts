import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../environments/environment";
import {Product} from "../models/product";
import {Order} from "../models/order";
import {AccountDetails, AccountDetailsAdapter} from "../models/dto/account-details";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient,
              private accountDetailsAdaptor: AccountDetailsAdapter) {
  }

  fetchAccountDetails(username: String): Observable<AccountDetails> {
    return this.httpClient.get<any>(
      `${environment.server}${environment.pathPrefix}${environment.resources.user}/${username}`
    ).pipe(map((data: any) => {
      return this.accountDetailsAdaptor.adapt(data);
    }));
  }

  fetchProducts(): Observable<Array<Product>> {
    return this.httpClient
      .get<any>(
        `${environment.server}${environment.pathPrefix}${environment.resources.products}`
      ).pipe(map((data: any) => {
        const {products} = data;

        return products;
      }));
  }

  sendOrder(order: Order): Observable<Order> {
    return this.httpClient
      .post<any>(
        `${environment.server}${environment.pathPrefix}${environment.resources.orders}`,
        {order: {items: order.items, user_id: order.userId}}
      ).pipe(map((data: any) => {
        const order: Order = {
          orderId: data.order.order_id,
          total: data.order.data.total || 0,
          items: data.order.data.items || []
        }
        return order;
      }))
  }
}
