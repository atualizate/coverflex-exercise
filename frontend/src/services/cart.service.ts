import {Injectable} from '@angular/core';
import {Cart} from "../models/cart";
import {Product} from "../models/product";
import {User} from "../models/user";
import {Order} from "../models/order";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart: Cart;

  constructor() {
    this.cart = new Cart();
  }

  get numberOfItems(): number {
    return this.cart.numberOfItems;
  }

  get total(): number {
    return this.cart.total;
  }

  products(): Array<Product> {
    return this.cart.products();
  }

  hasProduct(product: Product): boolean {
    return this.cart.hasProduct(product);
  }

  addProduct(product: Product) {
    this.cart.addProduct(product);
  }

  removeProduct(product: Product) {
    this.cart.removeProduct(product);
  }

  resetCart(): void {
    this.cart = new Cart();
  }

  newOrder(user: User): Order {
    return {
      userId: user.username,
      items: Array.from(this.cart.items.keys())
    }
  }

}
