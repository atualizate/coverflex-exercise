import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {Order} from "../models/order";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {Product} from "../models/product";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  private _alreadyOrdered: Set<string>;

  constructor(private apiService: ApiService) {
    this._alreadyOrdered = new Set<string>();
  }

  set alreadyOrdered(value: Set<string>) {
    this._alreadyOrdered = value;
  }

  hasProductId(product: Product): boolean {
    return this._alreadyOrdered.has(product.id);
  }

  public placeOrder(order: Order): Observable<boolean> {
    return this.apiService.sendOrder(order)
      .pipe(
        map((response) => {
          response
            .items
            .forEach(item => this._alreadyOrdered.add(item));
          return true;
        })
      );
  }

}
