#!/bin/bash

set -e

function usage() {
    cat <<EOF
Usage: ./start_backend [-rt]

-h          Display help and exit
-r          Start server at 8080 port
-t          Run tests
EOF

exit 0
}

while getopts ":rt" arg; do
    case ${arg} in
        r)
            RUN=1
        ;;
        t)
            TESTS=1
        ;;
        h)
            usage
        ;;
        *)
            usage
        ;;
    esac
done
shift $((OPTIND-1))

cd backend

if [[ ${TESTS} -eq 1 ]];
then
    ./mvnw test
    exit 0
fi


./mvnw spring-boot:run
